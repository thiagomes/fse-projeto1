# FSE_Projeto1

## Informações 
Nome: Thiago Luiz de Souza Gomes
Matrícula: 180028324
## Objetivo
- Realizar o controle da temperatura de um sistema de forma automática utilizando os conhecimentos obtidos nas aulas de Fundamentos de sistemas embarcados da UnB.
- A temperatura do sistema é controlada a partir da lógica PID.
- O programa pode ser controlado tanto pelo potenciômetro quanto pelo terminal.
 
## Como iniciar
- Utilizar o seguinte comando na pasta raíz do programa:
```
make
```
- Após o programa ser compilado, executar o binário que foi gerado no diretório *bin*.
```
./bin/bin
```
 
## Instruções
### Terminal
- Digite 1
- Siga as instruções na tela
### Potenciômetro
- Digite 2
- Siga as instruções na tela
### Importante
- Para desligar o sistema de forma segura utilize o comando:
```
   Ctrl + c
```
