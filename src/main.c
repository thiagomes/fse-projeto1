#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "bme280.h"
#include "display.h"
#include "gpio.h"
#include "pid.h"
#include "thermometer.h"
#include "uart.h"
#include "uart_defs.h"




struct bme280_dev bme_connection;
int uart_file;

void shutdown_program() {
  system("clear");
  printf("Programa encerrado\n");
  turn_resistance_off();
  turn_fan_off();
  close_uart(uart_file);
  exit(0);
}


void potentiometer() {
  system("clear");
  float hysteresis, TI, TR, TE;
  int send_value = 0;
  printf("\n---------------- Iniciando a rotina do UART -------------------\n");
  pid_setup_constants(20,0.1,100);
  do {
    write_uart_get(uart_file, GET_INTERNAL_TEMP);
    TI = read_uart(uart_file, GET_INTERNAL_TEMP).float_value;

    double send_value = pid_control(TI);

    pwm_control(send_value);

    write_uart_get(uart_file, GET_POTENTIOMETER);
    TR = read_uart(uart_file, GET_POTENTIOMETER).float_value;

    pid_update_reference(TR);

    TE = get_current_temperature(&bme_connection);
    printf("\tTI: %.2f⁰C - TR: %.2f⁰C - TE: %.2f⁰C\n", TI, TR, TE);

    print_display(TI, TR, TE);


    write_uart_send(uart_file, send_value);
  } while (1);
  printf("--------------------------------------------------------------------\n");
  
}

void terminal()
{
    system("clear");
    float TI;
    float TR;
    float TE;
    double kp;
    double ki;
    double kd;

    printf("\n-------------------- TERMINAL ------------------------------\n");
    printf("Insira o valor de TR: \n");
    scanf("%f", &TR);

    printf("Insira os valores de kp: \n");
    scanf("%f",&kp);

    printf("Insira os valores de ki: \n");
    scanf("%f",&ki);

    printf("Insira os valores de kd: \n");
    scanf("%f",&kd);
  

    pid_setup_constants(kp, ki, kd);

    int i = 0;
    while(1)
    {
        write_uart_get(uart_file, GET_INTERNAL_TEMP);
        TI = read_uart(uart_file, GET_INTERNAL_TEMP).float_value;

        double aux = pid_control(TI);

        pwm_control(aux);

        write_uart_get(uart_file, GET_POTENTIOMETER);
        pid_update_reference(TR);

        TE = get_current_temperature(&bme_connection);
        printf("\tTI: %.2f⁰C - TR: %.2f⁰C - TE: %.2f⁰C\n", TI, TR, TE);
        print_display(TI, TR, TE);
        i++;
        write_uart_send(uart_file, aux);
    }
  
    
    printf("-----------------------------------------------------------------------------------\n");
}



void init() {
  wiringPiSetup();
  turn_resistance_off();
  turn_fan_off();
  connect_display();
  bme_connection = connect_bme();
  uart_file = connect_uart();
  system("clear");
}

void menu() {
  int option, key;
  printf("Como deseja controlar o programa?\n\t1) Terminal\n\t2) Potenciometro\n");
  scanf("%d", &option);
  switch (option) {
    case 1:
      system("clear");
      terminal();
      break;
    case 2:
      system("clear");
      potentiometer();
      break;
    default:
      system("clear");
      printf("Opção invalida\n");
      menu();
      break;
  }
}

int main() {
  init();
  signal(SIGINT, shutdown_program);
  printf(
      "\n"
      "PROJETO 1 DE FSE\n"

      );

  menu();
  return 0;
}
